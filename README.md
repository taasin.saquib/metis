# pulumi-start

* example project to use pulumi to deploy a python function to GCP Compute Functions
* automate deployment with cicd pipeline

## Env Var
the pipeline requires 2 vars to be set
* PULUMI_ACCESS_TOKEN, which can be created from your pulumi account
* GOOGLE_CREDENTIALS, a service account key stored in file form
    * the service account has the following roles: `Cloud Functions Developer` and `Security Admin`

## ToDo
* the pipelines have to manually install virtual environments, and can be sped up if this was part of the docker image already
