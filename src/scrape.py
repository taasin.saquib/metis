import os
import json
import datetime, re, sys
import pytz
import time
import openpyxl
import json
import pymongo
import sys
import requests

from bs4 import BeautifulSoup
from pymongo import IndexModel, ASCENDING, DESCENDING
from openpyxl import Workbook
from sys import platform

# External Files
import preReqs
import dbase
import sel
from CCLE_professor_parser import CCLEHTMLParser
from scrape_soc import scrape_soc
from scrape_reg import scrape_reg
from scrape_ccle import scrape_ccle

def main():

    # connect to database
    client = dbase.connect()
    if client == "":
        return
    
    testing = False 

    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        testing = True
        print("Testing scrapers for Computer Science, not headless mode")

    sel.setup_driver(testing)

    # Scrape Class Descriptions
    # Not found: Civic Engagement, Labor and Workplace Studies, 
    scrape_reg(False, client)

    # Scrape the schedule of classes
    scrape_soc(False, client)

    # Scrape Professors
    scrape_ccle(False, client)

    client.close()
    sel.closeDriver()

if __name__ == '__main__':
    main()