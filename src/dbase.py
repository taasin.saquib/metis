# Database Related Functions

import pymongo
import pymongo.errors as dbErr

mongoURI = "mongodb+srv://tester:user@metis-eaoki.mongodb.net/test?retryWrites=true&w=majority"

def connect():
    """
    connect to Atlas MongoDB instance
    on success, return connection object
    on failure, return ""
    """
    
    client = ""
    
    try:
        client = pymongo.MongoClient(mongoURI)
    except dbErr.InvalidURI:
        print("ERROR connect() connecting to DB - Invalid URI")
    except dbErr.ConnectionFailure:
        print("ERROR connect() connecting to DB - Connection Failure")

    return client

def getMajors(client):
    """
    # return array of all majors 
    on success, return [string, ...]
    on failure, return []
    """
    
    majors = []

    # query database
    try:
        majorCollection = client.Metis.Departments
        cursor = majorCollection.find({})
        # extract the strings
        majors = [x["department_id"] for x in cursor]
    except:
        print("ERROR getMajors()- Couldn't get list of all majors")

    return majors

def getAcronyms(client):
    majors = []

    # query database
    try:
        majorCollection = client.Metis.MajorToAcronyms
        cursor = majorCollection.find({})
        majors = [x["acronym"] for x in cursor]
    except:
        print("ERROR getAcronyms()- Couldn't get list of all major acronyms")

    return majors

def getAcronym(client, major):
    """
        given computer science, return COM SCI
    """

    acronym = ""

    try:
        collection = client.Metis.MajorToAcronyms
        cursor = collection.find_one({"major": major})
        acronym = cursor["acronym"]
    except :
        print("ERROR getAcronym() - Couldn't get acronym for major: " + major)

    return acronym

# def updatePreReqs(client, pathways):