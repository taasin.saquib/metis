# Scrape Registrar - https://www.registrar.ucla.edu/Academics/Course-Descriptions

from bs4 import BeautifulSoup
from pymongo import IndexModel, ASCENDING, DESCENDING

import dbase
import sel
import re

url = "https://www.registrar.ucla.edu/Academics/Course-Descriptions"

def scrape_reg(testing, client):
    global url

    db = client.Scrape.Classes

    majors = []

    if testing:
        majors = ['Civic Engagement']
    else:
        majors = dbase.getMajors(client)
        if len(majors) == 0:
            return

    for major in majors:
        sel.navigate(url)

        # click on dropdown input
        found = False
        className = sel.getElements("//*[contains(text(), '" + major + "')]")
        
        for i in range(len(className)):
            majors = sel.getElements("//*[contains(text(), '" + major + "')]")
            sel.click(majors[i])

            driver = sel.getDriver()
            try:
                text = driver.execute_script("return document.getElementsByClassName('page-header')[1].innerHTML")
            except:
                sel.navigate(url)
                continue
            
            regex = r"({}) \(.*\)".format(major)
            majorSearch = re.search(regex, text)
            if majorSearch:
                pageMajor = majorSearch.group(1).strip()
                if pageMajor == major:
                    found = True
                    break
            
            # Might not have an abbreviation
            regex = r"({}) </span>".format(major)
            majorSearch = re.search(regex, text)
            if majorSearch:
                pageMajor = majorSearch.group(1).strip()
                if pageMajor == major:
                    found = True
                    break

            sel.navigate(url)
        
        if not found:
            print("ERROR: major {} not found".format(major))

        # Get the default course set (usually Lower Divs come up first)
        extract_descriptions(client, major, "lower")
        extract_descriptions(client, major, "upper")
        extract_descriptions(client, major, "graduate")

    # index composite key of (department, number)
    index1 = IndexModel([("department", ASCENDING), ("number", ASCENDING)], name="ClassesIndex")
    db.create_indexes([index1])

def extract_descriptions(client, dept, classType):
    """
    given a classType: lower, upper, graduate
    put class objects in db with descriptions
    """

    mapping = client.Metis.MajorToAcronyms
    collection = client.Scrape.Classes

    data = mapping.find_one({"major": dept})
    if data == None:
        
        driver = sel.getDriver()
        text = driver.execute_script("return document.getElementsByClassName('page-header')[1].innerHTML")
        regex = r"{} \((.*)\)".format(dept)
        abbrevSearch = re.search(regex, text)
        if abbrevSearch:
            abbrev = abbrevSearch.group(1).strip()
            mapping.insert_one({"major": dept, "acronym": abbrev})
            abbrevDept = abbrev
            print("Added new major to the db: {} - {}".format(dept, abbrev))
        else:
            abbrevDept = ""
            print("Major {} doesn't have an abbrevaition".format(dept))
            
    else:
        abbrevDept = data["acronym"]

    soup = sel.getHTML("div", {"id": classType})
    if soup == None:
        print("Error, cannot scrape Registrar Descriptions for " + dept + " " + classType)
        return

    classes = soup.find_all("li", {"class": "media category-list-item"})

    for c in classes:
        course_data = c.find("h3").get_text()
        course_text = course_data.split('.')

        units = ''
        desc = ''
        for p in c.find_all("p"):
            contents = p.get_text()
            if 'Units' not in contents:
                p_tags = p.get_text().split('.')
                for text in p_tags:
                    if 'hour' not in text and 'grading' not in text and text != '':
                        desc += (text + '.')

            else:
                unitSearch = re.search(r"Units: ([0-9]*.[0-9]*)$", contents)
                if unitSearch:
                    units = unitSearch.group(1).strip()

        # update DB
        myquery = { "department": abbrevDept, "number": course_text[0] }
        newvalues = { "$set": { "name": course_text[1], "description": desc, "units": units } }
        collection.update_one(myquery, newvalues, upsert=True)

        # print(e)
        return