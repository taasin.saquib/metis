# Scrape Schedule of Classes - https://sa.ucla.edu/ro/public/soc

import time
import requests
import re
from bs4 import BeautifulSoup

import dbase
import sel
import dependencies

url = "https://sa.ucla.edu/ro/public/soc"

def scrape_soc(testing, client):
    """
        testing [bool] - if you want to use one major to test functionality, set to true
        client [Object] - pymongo connection object
    """

    global url

    majorMap = {}   # store pre-reqs for each major here
    
    # Get List of All Majors
    majors = []

    if testing:
        majors = ['Computer Science']
    else:
        majors = dbase.getMajors(client)
        if len(majors) == 0:
            return

    for major in majors:
        if major in majorMap:
            if majorMap[major] != "Error":
                continue
        try:
            majorMap[major] = {}
            courseMap = majorMap[major]

            # get acronym
            acronym = dbase.getAcronym(client, major)
            if acronym == "":
                break

            # Go to schedule of classes page
            sel.navigate(url)

            # TODO: select term from dropdown, difficult to see the year and pick the right term

            # click on dropdown input
            subject_area_input = sel.check_exists_by_xpath("""//*[@id="select_filter_subject"]""")

            sel.scroll(subject_area_input)

            subject_area_input.click()

            # select the major
            class_dropdown = sel.check_exists_by_xpath("""//*[@id="select_filter_subject"]""")
            class_dropdown.click()
            state = sel.getElements("//*[contains(text(), '" + major + "')]")

            # Select the major from the dropdown
            for s in state:
                # only one of the returned elements is clickable, not sure which one so try all
                sel.click(s)
            sel.click(state[1])

            # Click the go button
            go = sel.check_exists_by_xpath("""//*[@id="btn_go"]""")
            go.click()

            # go through all pages of a major, if they exist
            lim = 0
            try:
                pages = sel.getElements("""//*[@class="jPag-pages"]""")
                li_list = pages[0].find_elements_by_xpath(".//*")
                li_list = li_list[::2]
                lim = len(li_list)
            except:
                lim = 1

            for i in range(0, lim):

                if lim != 1:
                    # repeat because the links have been refreshed
                    pages = sel.getElements("""//*[@class="jPag-pages"]""")
                    li_list = pages[0].find_elements_by_xpath(".//*")
                    li_list = li_list[::2]

                    # click twice, bug sometimes doesn't expand all classes otherwise
                    sel.click(li_list[i])
                    time.sleep(1)
                    sel.click(li_list[i])
                    time.sleep(1)

                # Click on the "expand all" button to see section information
                expand_classes = sel.check_exists_by_xpath("""//*[@id="expandAll"]""")
                sel.click(expand_classes)

                # Click on "lec1", "lab1", etc. to open new tab
                sections = sel.getElements("""//*[@class="hide-small"]""")    # get elements that hold the anchor tags
                # for each element found, get the anchor tag child
                section_links = []
                for s in sections:
                    section_links.append(s.find_elements_by_xpath(".//*"))

                # click on the anchor tags, switch to new tab, close it, switch back to original tab
                for section in section_links:

                    # don't need discussions to get pre-reqs

                    if ("Dis" not in section[0].text and "Tut" not in section[0].text) or ("Sem" in section[0].text and (section[0].text == "Sem 1")) or ("Lab" in section[0].text and (section[0].text == "Lab 1" or section[0].text == "Lab 1A")):
                        if ("Sem" in section[0].text and section[0].text!="Sem 1"):
                            continue
                        if ("Lab" in section[0].text and (section[0].text != "Lab 1" and section[0].text != "Lab 1A")):
                            continue

                        sel.click(section[0])

                        driver = sel.getDriver()
                        driver.switch_to.window(driver.window_handles[1])

                        # Get the current page's HTML
                        page_response = requests.get(driver.current_url, timeout=5)

                        # Get pre-reqs with getReqs()
                        courseTitle=re.search(r'subject_class[\s\S]*?( .*?) - ',page_response.text).group(1).strip()
                        courseTitle = " ".join(courseTitle.split())
                        courseTitle = courseTitle.replace("%26", "&")
                        courseTitle = courseTitle.replace("&amp;", "&")

                        # extract course num
                        regex = r".* (.*)$"
                        result = re.match(regex, courseTitle)
                        courseNum = result.group(1)

                        # print(acronym, courseNum)
                        
                        try:
                            pathways = Dependencies.getReqs(page_response.text)
                            courseMap[courseTitle] = pathways

                            myquery = { "department": acronym, "number": courseNum }
                            newvalues = { "$set": { "prerequisites": pathways } }
                            classCollection = client.Scrape.Classes

                            classCollection.update_one(myquery, newvalues, upsert=True)

                            driver.close()
                            driver.switch_to.window(driver.window_handles[0])
                        except:
                            courseMap[courseTitle] = "Error"
                            # time.sleep(2)
                            driver.close()
                            driver.switch_to.window(driver.window_handles[0])

                # TODO: extract info from the page about discussions, currently it returns JS and not HTML
                """
                # Get the current page's HTML
                page_response = requests.get(driver.current_url, timeout=5)

                # Create a soup
                soup = BeautifulSoup(page_response.content, "html.parser")
                classes = soup.find(id="divSearchResults")
                # print(classes.findChildren())
                """

        except:
            majorMap[major] = "Error"

    # For debug purposes
    # print(majorMap)