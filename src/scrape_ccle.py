# Scrape CCLE - https://ccle.ucla.edu/blocks/ucla_browseby/view.php?type=subjarea

import sel

import dbase
import CCLE_professor_parser as ccle

def scrape_ccle(testing, client):
    """
    cycle through quarters in each year for each major
    """

    acronyms = []

    if testing:
        acronyms = ['COM SCI']
    else:
        acronyms = dbase.getAcronyms(client)
        if len(acronyms) == 0:
            return

    quarterSymbols = ['W', 'S', '1', 'F']

    for a in acronyms:
        try:
            # replace spaces with '%20'
            abbrev = a.replace(' ', '%20')
            abbrev = a.replace('&', '%26')

            # go through each quarter in a range of years
            for i in range(19, 21):
                for q in quarterSymbols:
                    quarter = str(i) + q
                    # TODO: debug here
                    ccle_professor_scraper(client, abbrev, quarter, a)
        except:
            print("No acronym for: " + a)

def ccle_professor_scraper(client, abbrev, quarter, major):
    """
    major format: 'COM%20SCI', or 'AF%20AMER' etc.
    quarter format: '19F', '18W', etc.
    returns a list of (class id, professor, full class title) lists
    """

    url = 'https://ccle.ucla.edu/blocks/ucla_browseby/view.php?term={}&type=course&subjarea={}'.format(quarter, abbrev)
    sel.navigate(url)

    page_source = sel.getDriverSrc()
    ccle_professor_parser = ccle.CCLEHTMLParser()

    db = client.Scrape.ClassesByQuarter

    try:
        classes = ccle_professor_parser.get_class_professor_list(page_source)

        for c in classes:
            e = {
                "courseNum": c[0],
                "professor": c[1],
                "courseTitle": c[2],
                "major": major,
                "quarter": quarter
            }

            db.insert_one(e)
    except:
        print("CCLE doesn't have data for: " + major + " " + quarter)