"""A Google Cloud Python Pulumi program"""

from pulumi import export, asset
from pulumi_gcp import storage, cloudfunctions

# Create a GCP Bucket
bucket = storage.Bucket('fn-code')

# Export the DNS name of the bucket
export('bucket_name', bucket.url)

py_bucket_object = storage.BucketObject(
    "python-fn-zip",
    bucket=bucket.name,
    source=asset.AssetArchive({
        ".": asset.FileArchive("./src")
    }))

py_function = cloudfunctions.Function(
    "python-func",
    source_archive_bucket=bucket.name,
    runtime="python37",
    source_archive_object=py_bucket_object.name,
    entry_point="handler",
    trigger_http="true",
    available_memory_mb=128,
    region="us-central1",
)

py_invoker = cloudfunctions.FunctionIamMember(
    "py-invoker",
    project=py_function.project,
    region=py_function.region,
    cloud_function=py_function.name,
    role="roles/cloudfunctions.invoker",
    member="allUsers",
)

export("python_endpoint", py_function.https_trigger_url)